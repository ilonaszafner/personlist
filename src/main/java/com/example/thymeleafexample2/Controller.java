package com.example.thymeleafexample2;


import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@org.springframework.stereotype.Controller

public class Controller {

    public static List<Person> personList = new ArrayList<>();
    public List<Person> personList2 = new ArrayList<>();

  /*  @GetMapping("/")
    public String form(){
        return "form";
    }*/

    @GetMapping("/")
    public String addList(@ModelAttribute Person person, ModelMap map) {

        if (person.getName() != null && person.getSurname() != null) {
            personList.add(person);
        }
        // map.put("person", personList);
        return "form";
    }

    @GetMapping("/show")
    public String showAll(ModelMap map) {
        map.put("person", personList);
        return "show";
    }


    @GetMapping("/result")
    public String search(@RequestParam String surname, ModelMap map) {

        for (int i = 0; i < personList.size(); i++) {
            if (surname.equals(personList.get(i).getSurname())) {
                personList2.add(personList.get(i));
            }
            if (personList2.size() != 0)
                map.put("person", personList2);
        }
        map.put("person", personList2);
        return "result";
    }


    @GetMapping("/search")
    public String result() {

        return "search";
    }

}
