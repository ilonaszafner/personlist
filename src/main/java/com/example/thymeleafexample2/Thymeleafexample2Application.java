package com.example.thymeleafexample2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Thymeleafexample2Application {

    public static void main(String[] args) {
        SpringApplication.run(Thymeleafexample2Application.class, args);
    }
}
